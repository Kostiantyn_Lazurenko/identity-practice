﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { tap } from 'rxjs/operators';
import { AuthConstants } from './auth.constants';
import { User } from '../User/user';
import { UserService } from '../User/user.service';
import { environment } from 'src/environments/environment';
import { AppOAuthToken } from './app.oauth.token';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  public currentUser: BehaviorSubject<User>;
  public oauthToken: BehaviorSubject<AppOAuthToken>;

  constructor(
    private http: HttpClient,
    private userService: UserService) {
    this.currentUser = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.oauthToken = new BehaviorSubject<AppOAuthToken>(this.getOAuthTokenFromLocalStorage());
  }

  public get currentUserValue(): User {
    return this.currentUser.value;
  }

  public isUserAuthorized(): boolean {
    const oauthToken = this.getOAuthTokenFromLocalStorage();
    const isUserAuthorized = oauthToken !== null;
    return isUserAuthorized;
  }

  authenticateWithCredentials(username, password): Promise<void> {
    return this.http.post<any>(`${environment.apiUrl}/account/token`, { username, password })
      .toPromise()
      .then((authToken: AppOAuthToken) => {
        const oauthTokenData: any = {};
        oauthTokenData.id_token = authToken.access_token;
        this.updateOAuthToken(oauthTokenData);
        return this.setCurrentUser();
      });
  }

  public getOAuthTokenFromLocalStorage(): AppOAuthToken {
    const oauthTokenJson = localStorage.getItem(AuthConstants.oauthToken);
    if (oauthTokenJson === null) {
      return null;
    }

    const oauthToken: AppOAuthToken = JSON.parse(oauthTokenJson);
    return oauthToken;
  }

  public authenticateWithAccessCode(accessCode: string): Promise<void> {
    const url = `${environment.apiUrl}/oauth/microsoft/token?accessCode=${accessCode}&callbackUrl=${environment.authCallBackUrl}`;

    return this.http.get<AppOAuthToken>(url).toPromise().then((token: AppOAuthToken) => {
      this.updateOAuthToken(token);
      return this.setCurrentUser();
    });
  }

  private exchangeRefreshToken(refreshToken: string): Observable<AppOAuthToken> {
    const url = `${environment.apiUrl}/oauth/microsoft/refresh?refreshToken=${refreshToken}&callbackUrl=${environment.authCallBackUrl}`;

    return this.http.get<AppOAuthToken>(url)
      .pipe(
        tap((oauthToken: AppOAuthToken) => {
          this.updateOAuthToken(oauthToken);
        }),
        catchError((error) => {
          this.removeOAuthTokenFromLocalStorage();
          this.currentUser.next(null);
          return throwError(error);
        })
      );
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.removeOAuthTokenFromLocalStorage();
    this.currentUser.next(null);
  }

  setCurrentUser(): Promise<void> {
    return this.userService.getCurrentUser(this.currentUser.value != null ? this.currentUser.value.id : null).then((user: User) => {
      this.updateCurrentUser(user);
    });
  }

  public updateCurrentUser(user: User): void {
    localStorage.setItem('currentUser', JSON.stringify(user));
    this.currentUser.next(user);
  }

  private removeOAuthTokenFromLocalStorage(): void {
    localStorage.removeItem(AuthConstants.oauthToken);
  }

  private updateOAuthToken(oauthToken: AppOAuthToken) {
    localStorage.setItem(AuthConstants.oauthToken, JSON.stringify(oauthToken));
    this.oauthToken.next(oauthToken);
  }
}
