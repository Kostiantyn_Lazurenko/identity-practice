export class AuthConstants {
  public static readonly oauthToken = 'oauthToken';
  public static readonly authorizationCodeQueryParam = 'code';
  public static readonly sessionStateQueryParam = 'session_state';
}
