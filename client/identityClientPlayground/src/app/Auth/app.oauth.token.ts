export interface AppOAuthToken {
  access_token: string;
  username: string;
}
