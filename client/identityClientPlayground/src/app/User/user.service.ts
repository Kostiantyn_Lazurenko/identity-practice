import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from './user';

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<User[]>(`${environment.apiUrl}/user`);
  }

  register(user: User) {
    return this.http.post(`${environment.apiUrl}/api/user`, user);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}/api/user/${id}`);
  }

  public getCurrentUser(id: string): Promise<User> {

    const options = new HttpParams({
      fromObject: { id: id }
    });

    const url = `${environment.apiUrl}/api/user/currentUser`;
    return this.http.get<User>(url, {params: options}).toPromise();
  }
}
