export const environment = {
  production: true,
  apiUrl: 'https://localhost:44365',
  authCallBackUrl: 'http://localhost:4200/auth/callback'
};
