﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace IdentityCorePlayground.Auth
{
    public class AuthOptions
    {
        public const string Issuer = "ICServer";
        public const string Audience = "IPClient";
        public const int LifeTime = 1;
        private const string Key = "0912!qweSecret";

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}