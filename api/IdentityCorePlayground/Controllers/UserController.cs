﻿using IdentityCorePlayground.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityCorePlayground.Controllers
{
    [Route("api/user")]
    public class UserController : ControllerBase
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;

        public UserController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [HttpGet]
        public string Get()
        {
            return "Test";
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterModel registerModel)
        {
            var user = new User 
            { 
                UserName = registerModel.UserName, 
                FirstName = registerModel.FirstName, 
                LastName = registerModel.LastName 
            };

            var result = await this.userManager.CreateAsync(user, registerModel.Password);

            return result.Succeeded
                ? (IActionResult)this.Ok()
                : this.BadRequest(string.Join(Environment.NewLine, result.Errors.Select(m => m.Description)));
        }
    }
}
